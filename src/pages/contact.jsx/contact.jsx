import React from 'react';
import { Input } from '@nextui-org/react';
import { Textarea } from '@nextui-org/react';
import { Button } from '@nextui-org/react';

export default function Contact() {
  const handleSubmit = (e) => {
    e.preventDefault();
  };

  return (
    <div className='relative flex flex-col items-center justify-center h-screen'>
      <img
        alt='background'
        src='https://img.olympics.com/images/image/private/t_s_pog_staticContent_hero_xl_2x/f_auto/primary/ydk9vatpnihwfquy6zq3'
        className='object-cover w-full h-full aspect-video'
      ></img>
      <div className='absolute top-0 left-0 w-full h-full opacity-85 bg-gradient-to-b from-transparent to-black'></div>
      <section className='absolute overflow-y-auto left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 h-full w-full bg-black sm:w-[450px] sm:h-[80%] opacity-80'>
        <h1 className='flex justify-center mt-5 text-3xl font-bold'>Contract</h1>

        <div className='flex justify-center'>
          <form onSubmit={handleSubmit} className='p-6 mt-5'>
            <label htmlFor='name' className='text-white'>
              Name:
            </label>
            <Input label='Name' defaultValue='John Doe' />

            <label htmlFor='email' className='mt-4 text-white'>
              Email:
            </label>
            <Input
              type='email'
              label='Email'
              defaultValue='junior@nextui.org'
              description="We'll never share your email with anyone else."
            />

            <label htmlFor='message' className='mt-4 text-white'>
              Message:
            </label>
            <Textarea isRequired labelPlacement='outside' placeholder='Enter your description' />

            <Button
              type='submit'
              className='w-full px-4 py-2 mt-4 text-white bg-blue-500 rounded-md hover:bg-blue-600 focus:outline-none focus:bg-blue-600'
            >
              Submit
            </Button>
          </form>
        </div>
      </section>
    </div>
  );
}
