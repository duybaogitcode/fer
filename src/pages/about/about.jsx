import React from 'react';

export default function About() {
  return (
    <div className='relative flex flex-col items-center justify-center h-screen'>
      <img
        alt='background'
        src='https://wallpapers.com/images/hd/soccer-field-background-1920-x-1200-ko1b7myxotujjsjk.jpg'
        className='object-cover w-full h-full aspect-video'
      ></img>
      <div className='absolute top-0 left-0 w-full h-full opacity-85 bg-gradient-to-b from-transparent to-black'></div>
      <div className='absolute p-8 rounded-lg shadow-lg bg-modal'>
        <h1 className='flex justify-center mb-4 text-3xl font-bold'>About</h1>
        <p className='text-gray-700'>This is the about page of our website.</p>
      </div>
    </div>
  );
}
