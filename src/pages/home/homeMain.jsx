import React from 'react';
import PlayerCard from '../../components/card/myCard';
import { listOfPlayer } from '../../data/listOfPlayer';
import { useEffect } from 'react';
import axios from 'axios';

export default function HomeMain() {
  const items = listOfPlayer.map((item, index) => <PlayerCard item={item}></PlayerCard>);
  const shipment = {
    address_from: {
      district: '100100',
      city: '100000',
    },
    address_to: {
      district: '100300',
      city: '100000',
    },

    parcel: {
      cod: 500000,
      amount: 500000,
      width: 10,
      height: 10,
      length: 10,
      weight: 750,
    },
  };
  useEffect(() => {
    const api = async () => {
      const login = await axios.post('https://sandbox.goship.io/api/v2/login', {
        username: 'bao1322002@gmail.com',
        password: 'Duyb@o1302',
        client_id: 62,
        client_secret: 'ESVqcAKudePOSEO6j8tCKafD6aLdKaZF4tKqG6U2',
      });
      //s

      if (login) {
        const response = await axios.post(
          'http://sandbox.goship.io/api/v2/rates',
          {
            shipment: shipment,
          },
          {
            headers: {
              Authorization:
                'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ5YzkwZDkxMTc4OTJhZjg5ODlhNTgzMjY2YmNkYjg0YmEzMmI4NzcxMzBlY2E1YjVmMDZmYTNmZWM0NTY2YzdiMGEyMTg1MDNhZmUwY2JiIn0.eyJhdWQiOiIxMyIsImp0aSI6IjQ5YzkwZDkxMTc4OTJhZjg5ODlhNTgzMjY2YmNkYjg0YmEzMmI4NzcxMzBlY2E1YjVmMDZmYTNmZWM0NTY2YzdiMGEyMTg1MDNhZmUwY2JiIiwiaWF0IjoxNzA4OTUzNjU3LCJuYmYiOjE3MDg5NTM2NTcsImV4cCI6NDg2NDYyNzI1Nywic3ViIjoiMzI1MyIsInNjb3BlcyI6W119.cMSIisxK4U6GFZro-AvNymMCV27Bk80mqurOXsAEvwWDgl8qhACseUA2pqk4CYJKBrO05h45QnIzI9UEEd7BICwpj68R7YsDFNFini5kQL9aX7lgzoYYEJxpmxF1vGvjymBnTHoYYl6eKcNARnr2ez8xHVDFZtcL15S2Y6v3skJn_I8bPgaw3yPaVFofcDfQAVJB9GNexpklv4eEs7lM2jGctzdLcybHaUSSOfpJAfNaOkL79BfonUJqCTquZwKVQBvU5fta8dFWJKQtLr0F3KvZajGaNhVIXy4td4MhpGrwmNEZLvXM5PgaRU3CyAeA2WCM2t3Y732mr-lqNGwKE-U0b_7dXcj8ILzMUtjj7NXjJxarXD7A-P67Lps_3JfvcuUYrZm1AfdAACZbbRr04HiXSQ6jujwenZx8v2MaJrtiN77kaBhApZQkBtsCnHlf19I0a9YMfqdYTGU6kicuV0-ofpSOZ3sLqQK80dF728MM13JbcrhOfzhojPjLules5ejCAKXL2tUHsj0YbWmfrT-STOZVSEHwHuMRumheR7W8szWBSvuHFL2V_A2hDRR0BueZUdO25ZHaIVxU7P3RVWGrjU3MPdi2DxYOp85_eWftefcL-XLBYMHrXqdZ9ZVDuyM_0UiImK8t9ObHXGTWVKI2OVsvYzDEvpJ_EdibN5Y',
            },
          }
        );
        console.log(response);
      }
    };
    api(); // Gọi hàm api
  }, []);

  return (
    <div className='container mx-auto'>
      <h1 className='hidden mt-4 ml-3 text-3xl font-bold md:flex'>List of Players</h1>
      <div className='grid grid-cols-1 gap-5 px-4 mt-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4'>
        {items}
      </div>
    </div>
  );
}
