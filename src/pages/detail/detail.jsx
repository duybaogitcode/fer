import React from 'react';
import { listOfPlayer } from '../../data/listOfPlayer';
import { useParams } from 'react-router-dom';
import { Card, CardHeader, CardBody } from '@nextui-org/react';

export default function Detail() {
  const { id } = useParams();
  const player = listOfPlayer.find((player) => player.id === id);

  console.log(player);

  return (
    <div className='container mx-auto my-3'>
      <Card className='py-4 dark w-[80%] mx-auto '>
        <CardHeader className='flex-col items-start px-4 pt-2 pb-0'>
          <h1 className='font-bold uppercase text-large'>{player.name}</h1>
          <h3 className='text-medium'>Club: {player.club}</h3>
          {/* <img className='w-[10%]' src={player.logo} alt='logo'></img> */}
        </CardHeader>
        <CardBody className='grid grid-cols-3 py-2 mt-3 overflow-visible'>
          <img alt='Card background' className='object-cover rounded-xl w-[80%]' src={player.img} />

          <div className='space-y-4'>
            <p className='text-medium'>Position: {player.position}</p>
            <p className='text-medium'>Age: {player.age}</p>
            <p className='text-medium'>Height: {player.height}</p>
            <p className='text-medium'>Weight: {player.weight}</p>
          </div>
          <div className='space-y-4'>
            <p className='text-medium'>Shirt Number: {player.shirtNumber}</p>
            <p className='text-medium'>Nationality: {player.nationality}</p>
            <p className='text-medium'>Current team: {player.club}</p>
            <p className='text-medium'>Cost: {player.cost}$</p>
          </div>
        </CardBody>
        <CardBody>
          <h3 className='text-lg'>Description: {player.description}</h3>
          <br />
          <h3>Early career: {player.early_career}</h3>
          <br />
          <h3>Playing style: {player.playing_style}</h3>
          <br />
          <h3>Personal life: {player.personal_life}</h3>
          <br />
          <h3 className='text-lg'>Achievements: </h3>
          <ul className='list-disc list-inside'>
            {player.achievements.map((story, index) => (
              <li key={index} className='text-medium'>
                {story}
              </li>
            ))}
          </ul>
          <br></br>
          <h3 className='text-lg'>Career history: </h3>
          <ul className='list-disc list-inside'>
            {player.career_history.map((history, index) => (
              <li key={index} className='text-medium'>
                Club: {history.club} | Years: {history.years} | Goals: {history.goals}
              </li>
            ))}
          </ul>
          <br></br>
          <p>Club Games: {player.stats.club_games}</p>
          <br></br>
          <p>Club Goals: {player.stats.club_goals}</p>
          <br></br>

          <p>Country Games: {player.stats.country_games}</p>
          <br></br>

          <p>Country Goals: {player.stats.country_goals}</p>
        </CardBody>
      </Card>
    </div>
  );
}
