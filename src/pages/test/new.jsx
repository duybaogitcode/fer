import React from 'react';

export default function NewsPage() {
  return (
    <div className='flex items-center justify-center min-h-screen bg-gray-100'>
      <div className='p-8 bg-white rounded shadow'>
        <h1 className='mb-4 text-3xl font-bold'>News</h1>
        <p className='text-gray-700'>This is the news page</p>
      </div>
    </div>
  );
}
