const NotFound = () => (
  <div className='h-screen '>
    <h1 className='flex lg:hidden text-[150px] absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 items-center justify-center'>
      404
    </h1>
    <img
      className='object-cover w-full h-full'
      src='https://cdn.svgator.com/images/2022/01/cat.png'
      alt='404 Page'
    />
  </div>
);

export default NotFound;
