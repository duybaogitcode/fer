import React from 'react';

export default function News() {
  const newsData = [
    {
      title: 'Football News 1',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRHgPgvaa2IsN8xC0Hihbey4xvJUffgz5cWQ53TYBdSEkuVukcyDqeMNYF5hBk2-Z-RiRw&usqp=CAU',
    },
    {
      title: 'Football News 2',
      content: 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
      image: 'https://i.dailymail.co.uk/1s/2024/01/24/22/80433471-0-image-a-56_1706136867453.jpg',
    },
    {
      title: 'Football News 3',
      content:
        'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      image:
        'https://video.dailymail.co.uk/preview/mol/2024/01/24/5290308119971935205/964x580_JPG-SINGLE_5290308119971935205.jpg',
    },
  ];

  const trendingData = [
    {
      title: 'Trending News 1',
      content:
        'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae.',
      image:
        'https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/blt098f7e63560ec757/64f58a14742efa2d6b171128/TF_Window_Rank.jpg',
    },
    {
      title: 'Trending News 2',
      content:
        'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
      image: 'https://i.dailymail.co.uk/1s/2024/01/24/16/80417789-0-image-a-43_1706113818177.jpg',
    },
    {
      title: 'Trending News 3',
      content:
        'Nulla facilisi. Sed aliquam, nunc eget aliquet lacinia, odio ex ultricies arcu, vitae.',
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTm8h588zSnElG9pXo5JaRWrw-ZJd5zNy6eAg&usqp=CAU',
    },
  ];

  return (
    <div className='container mx-auto'>
      <h1 className='mt-3 mb-8 text-4xl font-bold'>Football News</h1>
      <div className='flex flex-col md:flex-row'>
        <div className='w-full md:w-3/4'>
          {newsData.map((news, index) => (
            <div key={index} className='mb-8'>
              <img src={news.image} alt={news.title} className='w-full md:w-[30%] h-auto mb-4' />
              <h2 className='mb-2 text-2xl font-bold'>{news.title}</h2>
              <p className='text-gray-600'>{news.content}</p>
            </div>
          ))}
        </div>
        <div className='w-full md:w-1/4 '>
          {trendingData.map((trending, index) => (
            <div key={index} className='mb-8'>
              <img
                src={trending.image}
                alt={trending.title}
                className='w-full md:w-[30%] h-auto mb-4'
              />
              <h2 className='mb-2 text-2xl font-bold'>{trending.title}</h2>
              <p className='text-gray-600'>{trending.content}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
