import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Footer from './components/footer.jsx/footer';
import Navigation from './components/navigation/navigation';
import HomeMain from './pages/home/homeMain';
import Detail from './pages/detail/detail';
import About from './pages/about/about';
import Contact from './pages/contact.jsx/contact';
import NotFound from './pages/error';
import News from './pages/news/news';
function App() {
  return (
    <div className='min-h-screen bg-black'>
      <BrowserRouter>
        <Navigation></Navigation>
        <Routes>
          <Route path='/' element={<HomeMain></HomeMain>}></Route>
          <Route path='/news' element={<News></News>}></Route>
          <Route path='/details/:id' element={<Detail></Detail>}></Route>
          <Route path='/about' element={<About></About>}></Route>
          <Route path='/contract' element={<Contact></Contact>}></Route>
          <Route path='*' element={<NotFound></NotFound>}></Route>
        </Routes>
        <Footer></Footer>
      </BrowserRouter>
    </div>
  );
}

export default App;
