export const listOfPlayer = [
  {
    id: '1',
    name: 'Cristiano Ronaldo',
    logo: 'https://upload.wikimedia.org/wikipedia/vi/9/9d/Logo_Al-Nassr.png',
    club: 'Al Nassr',
    img: 'https://library.sportingnews.com/styles/twitter_card_120x120/s3/2023-07/cristiano%20ronaldo%20al%20nassr%202023-2024.jpg?itok=bCKfLjOf',
    description:
      'Cristiano Ronaldo dos Santos Aveiro GOIH ComM is a Portuguese professional footballer who plays as a forward for Serie A club Juventus and captains the Portugal national team. With an illustrious career spanning decades, Ronaldo is renowned for his goal-scoring prowess, athleticism, and leadership on the field. His dedication to the sport has earned him numerous accolades, making him one of the greatest footballers of all time.',
    cost: 100000000,
    achievements: [
      'Scored a hat-trick in the Champions League final.',
      "Won the FIFA Ballon d'Or five times.",
      'Became the all-time leading scorer in the UEFA Champions League.',
      'Won the UEFA European Championship with Portugal in 2016.',
      'Scored a crucial last-minute goal to secure a comeback victory in a league match.',
      'Founded the CR7 Foundation to support children in need.',
    ],
    nationality: 'Portuguese',
    height: '187 cm',
    weight: '83 kg',
    shirtNumber: '7',
    position: 'Forward',
    age: 36,
    early_career:
      'Started his career at Sporting Lisbon youth academy, joined U16 team in 1997. Scored 44 goals in 31 matches playing for U16, U17 and U18.',

    career_history: [
      { club: 'Sporting Lisbon', years: '2002-2003', goals: 5 },
      { club: 'Man Utd', years: '2003-2009', goals: 118 },
      { club: 'Real Madrid', years: '2009-2018', goals: 450 },
      { club: 'Juventus', years: '2018-2021', goals: 101 },
      { club: 'Man Utd', years: '2021-2022', goals: 18 },
      { club: 'Al Nassr', years: '2022-', goals: 3 },
    ],

    stats: {
      club_games: 801,
      club_goals: 699,
      country_games: 191,
      country_goals: 117,
    },

    playing_style:
      'A player with tremendous pace, dribbling skills and technique. Can play as striker, winger or attacking midfielder.',

    personal_life:
      'Has 4 children with girlfriend Georgina Rodriguez. Actively involved in charitable activities.',
  },

  {
    id: '2',
    name: 'Lionel Messi',
    club: 'Inter Miami',
    img: 'https://cdnmedia.baotintuc.vn/Upload/EqV5H9rWgvy9oNikwkHLXA/files/17082023messi3.jpg',
    description:
      'Lionel Andrés Messi is an Argentine professional footballer who plays as a forward and captains both Spanish club Barcelona and the Argentina national team. Messi is widely regarded as one of the greatest footballers in the history of the sport, known for his incredible dribbling, precise finishing, and playmaking abilities. His consistency and numerous records make him a football icon.',
    cost: 120000000,
    achievements: [
      'Broke the record for most goals scored in a calendar year.',
      'Won the Copa America with Argentina.',
      'Named the Best FIFA Men’s Player multiple times.',
      'Scored a sensational solo goal in a crucial El Clásico match.',
      'Established the Leo Messi Foundation for charitable activities.',
    ],
    nationality: 'Argentine',
    height: '170 cm',
    weight: '72 kg',
    shirtNumber: '10',
    position: 'Forward',
    age: 34,
    early_career:
      'Joined Barcelona youth academy at age 13. Quickly progressed through ranks, leading Barcelona to wins at various youth levels.',

    career_history: [
      { club: 'Barcelona', years: '2004-2021', goals: 672 },
      { club: 'PSG', years: '2021-', goals: 23 },
    ],

    stats: {
      club_games: 778,
      club_goals: 695,
      country_games: 162,
      country_goals: 90,
    },

    playing_style:
      'Known for dribbling, playmaking, and ability to take on defenders. Plays as a forward or attacking midfielder.',

    personal_life:
      'Married with 3 children. Actively supports childrens charity and education programs.',
  },
  {
    id: '3',
    name: 'Neymar Jr',
    club: 'Al Hilal',
    img: 'https://thanhnien.mediacdn.vn/Uploaded/taynguyen/2022_10_15/000-32lf3lj-3566.jpg',
    description:
      'Neymar da Silva Santos Júnior, known as Neymar, is a Brazilian professional footballer who plays as a forward for Ligue 1 club Paris Saint-Germain and the Brazil national team. Neymar is renowned for his flair, creativity, and goal-scoring ability. His journey from Santos to Barcelona and later to PSG has been marked by numerous individual awards and team successes.',
    cost: 150000000,
    achievements: [
      'Won the UEFA Champions League with Barcelona.',
      'Scored the winning goal in the Olympic final.',
      'Named South American Footballer of the Year multiple times.',
      'Established the Neymar Jr. Institute to support underprivileged children.',
    ],
    nationality: 'Brazilian',
    height: '175 cm',
    weight: '68 kg',
    shirtNumber: '10',
    position: 'Forward',
    age: 29,
    career_history: [
      { club: 'Santos', years: '2009-2013', goals: 136 },
      { club: 'Barcelona', years: '2013-2017', goals: 105 },
      { club: 'PSG', years: '2017-', goals: 100 },
    ],

    stats: {
      club_games: 453,
      club_goals: 321,
      country_games: 116,
      country_goals: 70,
    },

    playing_style:
      'Skilled dribbler, known for flair and ability to beat defenders one-on-one. Plays as a forward.',

    personal_life:
      'Single, has a son born in 2011. Runs charitable foundation supporting children in need.',
  },
  {
    id: '4',
    name: 'Kylian Mbappe',
    club: 'Paris Saint-Germain (PSG)',
    img: 'https://image.vtc.vn/resize/th/upload/2022/12/18/argentina-26-23545330.jpg',
    description:
      'Kylian Mbappé Lottin is a French professional footballer who plays as a forward for Ligue 1 club Paris Saint-Germain and the France national team. Known for his incredible speed, skill, and clinical finishing, Mbappé has already achieved remarkable success at a young age, contributing significantly to PSG’s victories both domestically and in European competitions.',
    cost: 180000000,
    achievements: [
      'Became the youngest player to score in a World Cup final.',
      'Won the Golden Boy award.',
      'Named Ligue 1 Player of the Season multiple times.',
      'Engaged in charitable activities supporting education and healthcare.',
    ],
    nationality: 'French',
    height: '178 cm',
    weight: '73 kg',
    shirtNumber: '7',
    position: 'Forward',
    age: 23,
    early_career:
      'Started at AS Bondy youth academy. Joined Monaco academy in 2013, making first team debut aged 16.',

    career_history: [
      { club: 'Monaco', years: '2015-2017', goals: 64 },
      { club: 'PSG', years: '2017-', goals: 190 },
    ],

    stats: {
      club_games: 236,
      club_goals: 254,
      country_games: 59,
      country_goals: 28,
    },

    playing_style:
      'Pacey forward, excellent dribbler and clinical finisher. Dangerous on counter attacks.',

    personal_life:
      'In a relationship with actress Emma Smet. Participates in various charitable initiatives.',
  },
  {
    id: '5',
    name: 'Mohamed Salah',
    club: 'Liverpool',
    img: 'https://library.sportingnews.com/styles/crop_style_16_9_desktop/s3/2024-01/GettyImages-1936223431.jpg?h=56349eb4&itok=3c77zX-J',
    description:
      'Mohamed Salah Hamed Mahrous Ghaly is an Egyptian professional footballer who plays as a forward for Premier League club Liverpool and the Egypt national team. Salah, known for his goal-scoring exploits and versatility on the field, has played a crucial role in Liverpools recent successes, including their triumphs in the Premier League and the UEFA Champions League.',
    cost: 90000000,
    achievements: [
      'Won the Premier League Golden Boot twice.',
      'Scored the fastest goal in Champions League final history.',
      'Named CAF African Player of the Year multiple times.',
      'Contributed to charity initiatives supporting healthcare in Egypt.',
    ],
    nationality: 'Egyptian',
    height: '175 cm',
    weight: '71 kg',
    shirtNumber: '11',
    position: 'Forward',
    age: 29,
    early_career:
      'Began senior career with Egyptian club El Mokawloon in 2010 before moving to Europe.',

    career_history: [
      { club: 'Basel', years: '2012-2014', goals: 20 },
      { club: 'Chelsea', years: '2014-2016', goals: 19 },
      { club: 'Roma', years: '2016-2017', goals: 34 },
      { club: 'Liverpool', years: '2017-', goals: 118 },
    ],

    stats: {
      club_games: 439,
      club_goals: 198,
      country_games: 83,
      country_goals: 45,
    },

    playing_style:
      'Pacey winger, excellent at dribbling and link-up play. Dangerous scorer and creator.',

    personal_life:
      'Married with 2 children. Active in charitable work funding hospitals and schools in Egypt.',
  },
  {
    id: '6',
    name: 'Sadio Mane',
    club: 'Liverpool',
    img: 'https://yop.l-frii.com/wp-content/uploads/2023/12/Al-Nassr-les-fans-composent-un-chant-special-pour-Sadio-Mane.jpg',
    description:
      'Sadio Mané is a Senegalese professional footballer who plays as a winger for Premier League club Liverpool and the Senegal national team. Known for his pace, skill, and goal-scoring ability, Mane has been an integral part of Liverpools success in recent years, helping the team secure domestic and international titles.',
    cost: 80000000,
    achievements: [
      'Won the Premier League with Liverpool.',
      'Scored in the UEFA Super Cup final.',
      'Named CAF African Player of the Year.',
      'Initiated community development projects in Senegal.',
    ],
    nationality: 'Senegalese',
    height: '175 cm',
    weight: '69 kg',
    shirtNumber: '10',
    position: 'Winger',
    age: 29,
    early_career: 'Began playing for Generation Foot academy in Senegal at age 15.',

    career_history: [
      { club: 'Metz', years: '2011-2012', goals: 2 },
      { club: 'Red Bull Salzburg', years: '2012-2014', goals: 45 },
      { club: 'Southampton', years: '2014-2016', goals: 25 },
      { club: 'Liverpool', years: '2016-', goals: 90 },
    ],

    stats: {
      club_games: 369,
      club_goals: 162,
      country_games: 89,
      country_goals: 33,
    },

    playing_style: 'Pacey, energetic winger. Direct dribbling style and eye for goal.',

    personal_life:
      'Married with one child. Established a charitable foundation building schools in Senegal.',
  },
  {
    id: '7',
    name: 'Robert Lewandowski',
    club: 'Bayern Munich',
    img: 'https://pbs.twimg.com/media/GEhR0_lWcAAcJte.jpg',
    description:
      'Robert Lewandowski is a Polish professional footballer who plays as a striker for Bundesliga club Bayern Munich and is the captain of the Poland national team. Known for his clinical finishing and goal-scoring prowess, Lewandowski has been a consistent performer for both club and country, achieving numerous individual and team accolades.',
    cost: 110000000,
    achievements: [
      'Scored five goals in nine minutes in a Bundesliga match.',
      'Won the FIFA Club World Cup.',
      'Named Bundesliga Player of the Season multiple times.',
      'Supporter of charitable organizations promoting education in Poland.',
    ],
    nationality: 'Polish',
    height: '185 cm',
    weight: '79 kg',
    shirtNumber: '9',
    position: 'Striker',
    age: 33,
    arly_career:
      'Began career at Polish club Znicz Pruszków. Quickly rose through leagues, joining top Polish club Lech Poznań in 2008.',

    career_history: [
      { club: 'Lech Poznań', years: '2008-2010', goals: 41 },
      { club: 'Dortmund', years: '2010-2014', goals: 103 },
      { club: 'Bayern Munich', years: '2014-', goals: 238 },
    ],

    stats: {
      club_games: 640,
      club_goals: 472,
      country_games: 134,
      country_goals: 76,
    },

    playing_style:
      'Complete striker - aerial threat, clinical finisher, links up play. Excellent positional awareness.',

    personal_life:
      'Married with 2 children. Active in charity work promoting childrens education in Poland.',
  },
  {
    id: '8',
    name: 'Sergio Ramos',
    club: 'Real Madrid',
    img: 'https://media.baoquangninh.vn/upload/image/202306/medium/2099361_4f13a0dc92824c16a0e12339221bee1b.jpg',
    description:
      'Sergio Ramos García is a Spanish professional footballer who plays as a centre-back and captains both La Liga club Real Madrid and the Spain national team. Known for his leadership qualities, defensive skills, and ability to score crucial goals, Ramos has been a key figure in Real Madrids success over the years, contributing to numerous domestic and international triumphs.',
    cost: 70000000,
    achievements: [
      'Scored the winning goal in the 2014 UEFA Champions League final.',
      'Won the FIFA World Cup with Spain.',
      'Named in the UEFA Team of the Year multiple times.',
      'Active in charitable efforts supporting healthcare in Spain.',
    ],
    nationality: 'Spanish',
    height: '184 cm',
    weight: '82 kg',
    shirtNumber: '4',
    position: 'Centre-back',
    age: 35,
    early_career: 'Joined Sevilla youth academy aged 10, making first team debut in 2004 aged 18.',

    career_history: [
      { club: 'Sevilla', years: '2004-2005', goals: 2 },
      { club: 'Real Madrid', years: '2005-', goals: 71 },
    ],

    stats: {
      club_games: 671,
      club_goals: 101,
      country_games: 180,
      country_goals: 23,
    },

    playing_style:
      'Commanding centre-back, excellent in the air and strong tackler. Dangerous threat on set pieces.',

    personal_life:
      'Married with 4 children. Actively supports charitable healthcare programs in Spain.',
  },
  {
    id: '9',
    name: 'Kevin De Bruyne',
    club: 'Manchester City',
    img: 'https://baodongnai.com.vn/file/e7837c02876411cd0187645a2551379f/012024/14_2_20240114205802.jpg',
    description:
      'Kevin De Bruyne is a Belgian professional footballer who plays as a midfielder for Premier League club Manchester City and the Belgium national team. De Bruyne is recognized for his exceptional passing ability, vision, and goal-scoring prowess from midfield. His impact on the pitch has been instrumental in Manchester Citys domestic and international successes.',
    cost: 100000000,
    achievements: [
      'Won the Premier League Player of the Season award.',
      'Assisted the winning goal in the 2021 UEFA Champions League final.',
      'Named in the PFA Team of the Year multiple times.',
      'Supporter of initiatives promoting youth development in Belgium.',
    ],
    nationality: 'Belgian',
    height: '181 cm',
    weight: '70 kg',
    shirtNumber: '17',
    position: 'Midfielder',
    age: 30,
    early_career: 'Joined Genk academy at age 14, making first team debut in 2010 aged 19.',

    career_history: [
      { club: 'Genk', years: '2010-2012', goals: 16 },
      { club: 'Chelsea', years: '2012-2014', goals: 2 },
      { club: 'Wolfsburg', years: '2014-2015', goals: 10 },
      { club: 'Man City', years: '2015-', goals: 96 },
    ],

    stats: {
      club_games: 363,
      club_goals: 124,
      country_games: 93,
      country_goals: 23,
    },

    playing_style:
      'World-class playmaker, excellent vision and range of passing. Dangerous striker of the ball.',

    personal_life: 'Married with 3 children. Supports youth football programs in Belgium.',
  },
  {
    id: '10',
    name: 'Virgil van Dijk',
    club: 'Liverpool',
    img: 'https://i.guim.co.uk/img/media/ed6f4843345f6d874e607482bb8fa4817306cff5/0_118_2952_1772/master/2952.jpg?width=1200&height=900&quality=85&auto=format&fit=crop&s=23c5f18c58f3ac5a77f5d5c9e55e7a2b',
    description:
      'Virgil van Dijk is a Dutch professional footballer who plays as a centre-back for Premier League club Liverpool and captains the Netherlands national team. Van Dijk is widely regarded as one of the best defenders in the world, known for his composure, leadership, and aerial prowess. His impact on Liverpools defense has been crucial to the teams success in recent years.',
    cost: 90000000,
    achievements: [
      'Won the UEFA Champions League with Liverpool.',
      "Named UEFA Men's Player of the Year.",
      'Named in the FIFPro World XI multiple times.',
      'Active in community initiatives supporting youth football development in the Netherlands.',
    ],
    nationality: 'Dutch',
    height: '193 cm',
    weight: '92 kg',
    shirtNumber: '4',
    position: 'Centre-back',
    age: 30,
    early_career: 'Began senior career with Dutch club Groningen, becoming captain aged just 21.',

    career_history: [
      { club: 'Groningen', years: '2011-2013', goals: 6 },
      { club: 'Celtic', years: '2013-2015', goals: 8 },
      { club: 'Southampton', years: '2015-2018', goals: 8 },
      { club: 'Liverpool', years: '2018-', goals: 14 },
    ],

    stats: {
      club_games: 255,
      club_goals: 36,
      country_games: 43,
      country_goals: 5,
    },

    playing_style:
      'Dominant centre-back, excellent in the air and strong tackler. Calm presence organizing defense.',

    personal_life:
      'Married. Active in community outreach programs promoting youth football in the Netherlands.',
  },
];
