import React from 'react';
import { Card, CardFooter, Button } from '@nextui-org/react';
import { useDisclosure } from '@nextui-org/react';
import PlayerModel from '../modals/playerModel';

export default function PlayerCard(item) {
  const { isOpen, onOpen, onOpenChange } = useDisclosure();
  console.log(item.item.n);

  return (
    <Card isFooterBlurred radius='lg' className='border-none'>
      <img
        alt='Woman listing to music'
        className='object-cover w-full h-full'
        src={item.item.img}
      />
      <CardFooter className='justify-between before:bg-white/10 border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10'>
        <p className='text-tiny text-white/80'>
          {item.item.name} - {item.item.club}
        </p>
        <Button
          className='text-white text-tiny bg-black/20'
          variant='flat'
          color='default'
          radius='lg'
          size='sm'
          onClick={onOpen}
        >
          Detail
        </Button>
      </CardFooter>
      <PlayerModel isOpen={isOpen} onOpenChange={onOpenChange} item={item.item}></PlayerModel>
    </Card>
  );
}
