import React, { useState } from 'react';
import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  Button,
  NavbarMenuToggle,
  NavbarMenu,
  NavbarMenuItem,
} from '@nextui-org/react';
import { Link } from 'react-router-dom';

export default function Navigation() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const menuItems = [
    { title: 'Home', link: '/' },
    { title: 'News', link: '/news' },
    { title: 'About', link: '/about' },
    { title: 'Contract', link: '/contract' },
  ];

  return (
    <div>
      <Navbar className='border-gray-700 dark border-b-1' onMenuOpenChange={setIsMenuOpen}>
        <NavbarContent>
          <NavbarMenuToggle
            aria-label={isMenuOpen ? 'Close menu' : 'Open menu'}
            className='sm:hidden'
          />
          <NavbarBrand>
            <p className='font-bold text-white text-inherit'> LAB1</p>
          </NavbarBrand>
        </NavbarContent>

        <NavbarContent className='hidden gap-4 sm:flex' justify='center'>
          <NavbarItem isActive>
            <Link color='foreground' to={'/'}>
              Home
            </Link>
          </NavbarItem>

          <NavbarItem>
            <Link color='foreground' to={'/news'}>
              News
            </Link>
          </NavbarItem>
          <NavbarItem>
            <Link color='foreground' to={'/about'}>
              About
            </Link>
          </NavbarItem>
          <NavbarItem>
            <Link color='foreground' to={'contract'}>
              Contract
            </Link>
          </NavbarItem>
        </NavbarContent>
        <NavbarContent justify='end'>
          {/* <NavbarItem className='hidden lg:flex'>
            <Link href='#'>Log</Link>
          </NavbarItem> */}
          <NavbarItem>
            <Button as={Link} color='primary' href='#' variant='flat'>
              Sign Up
            </Button>
          </NavbarItem>
        </NavbarContent>
        <NavbarMenu className='dark'>
          {menuItems.map((item, index) => (
            <NavbarMenuItem key={`${item}-${index}`}>
              <Link
                color={
                  index === 2 ? 'primary' : index === menuItems.length - 1 ? 'danger' : 'foreground'
                }
                className='w-full'
                to={item.link}
                size='lg'
              >
                {item.title}
              </Link>
            </NavbarMenuItem>
          ))}
        </NavbarMenu>
      </Navbar>
    </div>
  );
}
