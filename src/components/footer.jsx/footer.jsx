export default function Footer() {
  return (
    <div className='w-full py-3 mt-3 border-t border-gray-800 '>
      <div className='container mx-auto'>
        <h3 className='text-center'>copyright @ 2022</h3>
      </div>
    </div>
  );
}
