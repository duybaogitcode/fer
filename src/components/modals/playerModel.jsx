import {
  Button,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
} from '@nextui-org/react';
import { Link } from 'react-router-dom';

export default function PlayerModel({ isOpen, onOpenChange, item }) {
  return (
    <div>
      <Modal
        isOpen={isOpen}
        onOpenChange={onOpenChange}
        isDismissable={false}
        size='4xl'
        scrollBehavior='outside'
      >
        <ModalContent className='dark'>
          {(onClose) => (
            <>
              <ModalHeader className='flex flex-col gap-1'>{item.name}</ModalHeader>
              <ModalBody>
                <img alt='Woman listing to music' src={item.img} className='object-cover' />
                <h3>{item.club}</h3>
                <p>{item.description}</p>
              </ModalBody>
              <ModalFooter>
                <Button color='danger' variant='light' onPress={onClose}>
                  Close
                </Button>
                <Link to={`/details/${item.id}`}>
                  <div className='px-3 py-2 bg-blue-500 rounded-lg' color='primary'>
                    Go to details
                  </div>
                </Link>
              </ModalFooter>
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
}
